// console.log('Hello World');

const posts = [];
let count = 1;

const addPostForm = document.querySelector('#form-add-post');
const title = document.querySelector('#title');
const body = document.querySelector('#body');
const div = document.querySelector('#div-post-entries');
addPostForm.addEventListener('submit', (event) => {
  // prevent the automatic reload of page when submitting
  event.preventDefault();
  const post = {
    id: count,
    title: title.value,
    body: body.value,
  };
  posts.push(post);
  console.log(posts);

  count++;
  showPosts(posts);
});

// show posts

const showPosts = (posts) => {
  let postEntries = '';

  posts.forEach((post) => {
    postEntries += `
    <div id="post-${post.id}">
    <h3 id="post-title-${post.id}">${post.title}</h3>
    <p id="post-body-${post.id}">${post.body}</p><br>
    <button onClick = "editPost('${post.id}')"> Edit </button>
    <button onClick = "deletePost('${post.id}')"> Delete </button>
    </div>
    `;
  });
  console.log(postEntries);
  div.innerHTML = postEntries;
};

// Edit post

const editPost = (id) => {
  const titlePost = document.querySelector(`#post-title-${id}`).innerHTML;
  const bodyPost = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector('#edit-title').value = titlePost;
  document.querySelector('#edit-body').value = bodyPost;
  document.querySelector('#edit-id').value = id;
};

// Delete post
const deletePost = (id) => {
  console.log('delete');
  for (let index = 0; index < posts.length; index++) {
    console.log(posts[index].id);
    console.log(id);
    if (posts[index].id.toString() === id) {
      posts.splice(index, 1);

      showPosts(posts);
      alert('Post Deleted');
      break;
    }
  }
};

// Update
const editForm = document.querySelector('#form-edit-post');

editForm.addEventListener('submit', (event) => {
  event.preventDefault();
  const isToEdit = document.querySelector('#edit-id').value;
  for (let index = 0; index < posts.length; index++) {
    if (posts[index].id.toString() === isToEdit) {
      posts[index].title = document.querySelector('#edit-title').value;
      posts[index].body = document.querySelector('#edit-body').value;

      showPosts(posts);
      alert('Post Updated');
      break;
    }
  }
});
